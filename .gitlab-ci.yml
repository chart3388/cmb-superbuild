include:
    # Metadata shared my many jobs
    - local: .gitlab/rules.yml
    - local: .gitlab/artifacts.yml

    # OS builds.
    - local: .gitlab/os-linux.yml
    - local: .gitlab/os-macos.yml
    - local: .gitlab/os-windows.yml

    # Post-build steps
    - local: .gitlab/upload.yml
    - local: .gitlab/update-ci.yml

stages:
    - build
    - test
    - upload
    - update-ci

################################################################################
# Job declarations
#
# Each job must pull in each of the following keys:
#
#   - a "base image"
#   - a build script
#   - tags for the jobs
#     - already provided for upload and CI update jobs
#   - rules for when to run the job
#
# Additionally, jobs may also contain:
#
#   - artifacts
#   - dependency/needs jobs for required jobs
################################################################################

# CMB builds

## Linux

### CentOS 7 (Modelbuilder)

build:centos7-modelbuilder:
    extends:
        - .centos7_modelbuilder
        - .cmake_build_linux
        - .cmake_build_artifacts
        - .linux_builder_tags
        - .run_manually

test:centos7-modelbuilder:
    extends:
        - .centos7_modelbuilder
        - .cmake_test_linux
        - .cmake_package_artifacts
        - .linux_test_tags
        - .run_automatically
    dependencies:
        - build:centos7-modelbuilder
    needs:
        - build:centos7-modelbuilder

upload:girder-linux-modelbuilder:
    extends:
        - .girder_upload
        - .upload_only
    variables:
        # ComputationalModelBuilder/CMB-public/Nightly/MasterBranch
        GIRDER_TARGET_FOLDER: 5980ed698d777f16d01ea0e8
        GIT_CLONE_PATH: $CI_BUILDS_DIR/gitlab-kitware-sciviz-ci
    dependencies:
        - test:centos7-modelbuilder
    needs:
        - test:centos7-modelbuilder

### VTK-only

build:centos7-vtk:
    extends:
        - .centos7_vtk
        - .cmake_build_linux
        - .cmake_build_artifacts
        - .linux_builder_tags
        - .run_manually

test:centos7-vtk:
    extends:
        - .centos7_vtk
        - .cmake_test_linux
        - .linux_test_tags
        - .run_automatically
    dependencies:
        - build:centos7-vtk
    needs:
        - build:centos7-vtk

## macOS

### Modelbuilder

build:macos-arm64-modelbuilder:
    extends:
        - .macos_arm64_modelbuilder
        - .cmake_build_macos
        - .cmake_build_artifacts
        - .macos_arm64_builder_tags
        - .run_manually

test:macos-arm64-modelbuilder:
    extends:
        - .macos_arm64_modelbuilder
        - .cmake_test_macos
        - .cmake_package_artifacts
        - .macos_arm64_builder_tags
        - .run_automatically
    dependencies:
        - build:macos-arm64-modelbuilder
    needs:
        - build:macos-arm64-modelbuilder

upload:girder-macos-arm64-modelbuilder:
    extends:
        - .girder_upload
        - .upload_only
    variables:
        # ComputationalModelBuilder/CMB-public/Nightly/MasterBranch
        GIRDER_TARGET_FOLDER: 5980ed698d777f16d01ea0e8
        GIT_CLONE_PATH: $CI_BUILDS_DIR/gitlab-kitware-sciviz-ci
    dependencies:
        - test:macos-arm64-modelbuilder
    needs:
        - test:macos-arm64-modelbuilder

build:macos-x86_64-modelbuilder:
    extends:
        - .macos_x86_64_modelbuilder
        - .cmake_build_macos
        - .cmake_build_artifacts
        - .macos_builder_tags
        - .run_manually

test:macos-x86_64-modelbuilder:
    extends:
        - .macos_x86_64_modelbuilder
        - .cmake_test_macos
        - .cmake_package_artifacts
        - .macos_builder_tags
        - .run_automatically
    dependencies:
        - build:macos-x86_64-modelbuilder
    needs:
        - build:macos-x86_64-modelbuilder

upload:girder-macos-x86_64-modelbuilder:
    extends:
        - .girder_upload
        - .upload_only
    variables:
        # ComputationalModelBuilder/CMB-public/Nightly/MasterBranch
        GIRDER_TARGET_FOLDER: 5980ed698d777f16d01ea0e8
        GIT_CLONE_PATH: $CI_BUILDS_DIR/gitlab-kitware-sciviz-ci
    dependencies:
        - test:macos-x86_64-modelbuilder
    needs:
        - test:macos-x86_64-modelbuilder

### SMTK-only

build:macos-arm64-smtk:
    extends:
        - .macos_arm64_smtk
        - .cmake_build_macos
        - .cmake_build_artifacts
        - .macos_arm64_builder_tags
        - .run_manually

test:macos-arm64-smtk:
    extends:
        - .macos_arm64_smtk
        - .cmake_test_macos
        - .macos_arm64_builder_tags
        - .run_automatically
    dependencies:
        - build:macos-arm64-smtk
    needs:
        - build:macos-arm64-smtk

build:macos-x86_64-smtk:
    extends:
        - .macos_x86_64_smtk
        - .cmake_build_macos
        - .cmake_build_artifacts
        - .macos_builder_tags
        - .run_manually

test:macos-x86_64-smtk:
    extends:
        - .macos_x86_64_smtk
        - .cmake_test_macos
        - .macos_builder_tags
        - .run_automatically
    dependencies:
        - build:macos-x86_64-smtk
    needs:
        - build:macos-x86_64-smtk

## Windows

### Modelbuilder

build:windows-vs2019-modelbuilder:
    extends:
        - .windows_vs2019_modelbuilder
        - .cmake_build_windows
        - .cmake_build_artifacts
        - .windows_builder_tags
        - .run_manually
    # adding occt extends the build time
    timeout: 1h 30m

test:windows-vs2019-modelbuilder:
    extends:
        - .windows_vs2019_modelbuilder
        - .cmake_test_windows
        - .cmake_package_artifacts
        - .windows_builder_tags
        - .run_automatically
    dependencies:
        - build:windows-vs2019-modelbuilder
    needs:
        - build:windows-vs2019-modelbuilder

upload:girder-windows-vs2019-modelbuilder:
    extends:
        - .girder_upload
        - .upload_only
    variables:
        # ComputationalModelBuilder/CMB-public/Nightly/MasterBranch
        GIRDER_TARGET_FOLDER: 5980ed698d777f16d01ea0e8
        GIT_CLONE_PATH: $CI_BUILDS_DIR/gitlab-kitware-sciviz-ci
    dependencies:
        - test:windows-vs2019-modelbuilder
    needs:
        - test:windows-vs2019-modelbuilder

### SMTK-only

build:windows-vs2019-smtk:
    extends:
        - .windows_vs2019_smtk
        - .cmake_build_windows
        - .cmake_build_artifacts
        - .windows_builder_tags
        - .run_manually

test:windows-vs2019-smtk:
    extends:
        - .windows_vs2019_smtk
        - .cmake_test_windows
        - .windows_builder_tags
        - .run_automatically
    dependencies:
        - build:windows-vs2019-smtk
    needs:
        - build:windows-vs2019-smtk

# CMB-2D builds

## Linux

### CentOS 7 (CMB-2D)

build:centos7-cmb2d:
    extends:
        - .centos7_cmb2d
        - .cmake_build_linux
        - .cmake_build_artifacts
        - .linux_builder_tags
        - .run_manually

test:centos7-cmb2d:
    extends:
        - .centos7_cmb2d
        - .cmake_test_linux
        - .cmake_package_artifacts
        - .linux_test_tags
        - .run_automatically
    dependencies:
        - build:centos7-cmb2d
    needs:
        - build:centos7-cmb2d

upload:girder-linux-cmb2d:
    extends:
        - .girder_upload
        - .upload_only
    variables:
        # CMB-2D/continuous
        GIRDER_TARGET_FOLDER: 612d55022fa25629b99337b0
        GIT_CLONE_PATH: $CI_BUILDS_DIR/gitlab-kitware-sciviz-ci
    dependencies:
        - test:centos7-cmb2d
    needs:
        - test:centos7-cmb2d

# Truchas builds

## Linux

### CentOS 7 (Truchas)

build:centos7-truchas:
    extends:
        - .centos7_truchas
        - .cmake_build_linux
        - .cmake_build_artifacts
        - .linux_builder_tags
        - .run_manually

test:centos7-truchas:
    extends:
        - .centos7_truchas
        - .cmake_test_linux
        - .cmake_package_artifacts
        - .linux_test_tags
        - .run_automatically
    dependencies:
        - build:centos7-truchas
    needs:
        - build:centos7-truchas

upload:girder-linux-truchas:
    extends:
        - .girder_upload
        - .upload_only
    variables:
        # Truchas/continuous
        GIRDER_TARGET_FOLDER: 60ad4fac2fa25629b9adf410
        GIT_CLONE_PATH: $CI_BUILDS_DIR/gitlab-kitware-sciviz-ci
    dependencies:
        - test:centos7-truchas
    needs:
        - test:centos7-truchas

## macOS

### Truchas

build:macos-arm64-truchas:
    extends:
        - .macos_arm64_truchas
        - .cmake_build_macos
        - .cmake_build_artifacts
        - .macos_arm64_builder_tags
        - .run_manually

test:macos-arm64-truchas:
    extends:
        - .macos_arm64_truchas
        - .cmake_test_macos
        - .cmake_package_artifacts
        - .macos_arm64_builder_tags
        - .run_automatically
    dependencies:
        - build:macos-arm64-truchas
    needs:
        - build:macos-arm64-truchas

upload:girder-macos-arm64-truchas:
    extends:
        - .girder_upload
        - .upload_only
    variables:
        # ComputationalModelBuilder/CMB-public/Truchas/Develop/CI
        GIRDER_TARGET_FOLDER:  60ad4fac2fa25629b9adf410
        GIT_CLONE_PATH: $CI_BUILDS_DIR/gitlab-kitware-sciviz-ci
    dependencies:
        - test:macos-arm64-truchas
    needs:
        - test:macos-arm64-truchas

build:macos-x86_64-truchas:
    extends:
        - .macos_x86_64_truchas
        - .cmake_build_macos
        - .cmake_build_artifacts
        - .macos_builder_tags
        - .run_manually

test:macos-x86_64-truchas:
    extends:
        - .macos_x86_64_truchas
        - .cmake_test_macos
        - .cmake_package_artifacts
        - .macos_builder_tags
        - .run_automatically
    dependencies:
        - build:macos-x86_64-truchas
    needs:
        - build:macos-x86_64-truchas

upload:girder-macos-x86_64-truchas:
    extends:
        - .girder_upload
        - .upload_only
    variables:
        # ComputationalModelBuilder/CMB-public/Truchas/Develop/CI
        GIRDER_TARGET_FOLDER:  60ad4fac2fa25629b9adf410
        GIT_CLONE_PATH: $CI_BUILDS_DIR/gitlab-kitware-sciviz-ci
    dependencies:
        - test:macos-x86_64-truchas
    needs:
        - test:macos-x86_64-truchas

## Windows

### Truchas

build:windows-vs2019-truchas:
    extends:
        - .windows_vs2019_truchas
        - .cmake_build_windows
        - .cmake_build_artifacts
        - .windows_builder_tags
        - .run_manually
    timeout: 1h 30m

test:windows-vs2019-truchas:
    extends:
        - .windows_vs2019_truchas
        - .cmake_test_windows
        - .cmake_package_artifacts
        - .windows_builder_tags
        - .run_automatically
    dependencies:
        - build:windows-vs2019-truchas
    needs:
        - build:windows-vs2019-truchas

upload:girder-windows-vs2019-truchas:
    extends:
        - .girder_upload
        - .upload_only
    variables:
        # Truchas/continuous
        GIRDER_TARGET_FOLDER: 60ad4fac2fa25629b9adf410
        GIT_CLONE_PATH: $CI_BUILDS_DIR/gitlab-kitware-sciviz-ci
    dependencies:
        - test:windows-vs2019-truchas
    needs:
        - test:windows-vs2019-truchas

# ACE3P builds

## Linux

### CentOS 7 (ACE3P)

build:centos7-ace3p:
    extends:
        - .centos7_ace3p
        - .cmake_build_linux
        - .cmake_build_artifacts
        - .linux_builder_tags
        - .run_manually

test:centos7-ace3p:
    extends:
        - .centos7_ace3p
        - .cmake_test_linux
        - .cmake_package_artifacts
        - .linux_test_tags
        - .run_automatically
    dependencies:
        - build:centos7-ace3p
    needs:
        - build:centos7-ace3p

upload:girder-linux-ace3p:
    extends:
        - .girder_upload
        - .upload_only
    variables:
        # ACE3P/continuous
        GIRDER_TARGET_FOLDER: 60ad4ed02fa25629b9adf3cf
        GIT_CLONE_PATH: $CI_BUILDS_DIR/gitlab-kitware-sciviz-ci
    dependencies:
        - test:centos7-ace3p
    needs:
        - test:centos7-ace3p

## macOS

### ACE3P

build:macos-x86_64-ace3p:
    extends:
        - .macos_x86_64_ace3p
        - .cmake_build_macos
        - .cmake_build_artifacts
        - .macos_builder_tags
        - .run_manually

test:macos-x86_64-ace3p:
    extends:
        - .macos_x86_64_ace3p
        - .cmake_test_macos
        - .cmake_package_artifacts
        - .macos_builder_tags
        - .run_automatically
    dependencies:
        - build:macos-x86_64-ace3p
    needs:
        - build:macos-x86_64-ace3p

upload:girder-macos-x86_64-ace3p:
    extends:
        - .girder_upload
        - .upload_only
    variables:
        # ACE3P/continuous
        GIRDER_TARGET_FOLDER: 60ad4ed02fa25629b9adf3cf
        GIT_CLONE_PATH: $CI_BUILDS_DIR/gitlab-kitware-sciviz-ci
    dependencies:
        - test:macos-x86_64-ace3p
    needs:
        - test:macos-x86_64-ace3p

## Windows

### ACE3P

build:windows-vs2019-ace3p:
    extends:
        - .windows_vs2019_ace3p
        - .cmake_build_windows
        - .cmake_build_artifacts
        - .windows_builder_tags
        - .run_manually
    # Machines are right on the edge 1 hour; avoid unnecessary rebuilds.
    timeout: 1h 30m

test:windows-vs2019-ace3p:
    extends:
        - .windows_vs2019_ace3p
        - .cmake_test_windows
        - .cmake_package_artifacts
        - .windows_builder_tags
        - .run_automatically
    dependencies:
        - build:windows-vs2019-ace3p
    needs:
        - build:windows-vs2019-ace3p

upload:girder-windows-vs2019-ace3p:
    extends:
        - .girder_upload
        - .upload_only
    variables:
        # ACE3P/continuous
        GIRDER_TARGET_FOLDER: 60ad4ed02fa25629b9adf3cf
        GIT_CLONE_PATH: $CI_BUILDS_DIR/gitlab-kitware-sciviz-ci
    dependencies:
        - test:windows-vs2019-ace3p
    needs:
        - test:windows-vs2019-ace3p

# AEVA builds

## Linux

### CentOS 7 (AEVA)

build:centos7-aeva:
    extends:
        - .centos7_aeva
        - .cmake_build_linux
        - .cmake_build_artifacts
        - .linux_builder_tags
        - .run_manually

test:centos7-aeva:
    extends:
        - .centos7_aeva
        - .cmake_test_linux
        - .cmake_package_artifacts
        - .linux_test_tags
        - .run_automatically
    dependencies:
        - build:centos7-aeva
    needs:
        - build:centos7-aeva

upload:girder-linux-aeva:
    extends:
        - .girder_upload
        - .upload_only
    variables:
        # AEVA/continuous
        GIRDER_TARGET_FOLDER: 5e7a46f6af2e2eed356a1836
        GIT_CLONE_PATH: $CI_BUILDS_DIR/gitlab-kitware-sciviz-ci
    dependencies:
        - test:centos7-aeva
    needs:
        - test:centos7-aeva

## macOS

### AEVA

build:macos-x86_64-aeva:
    extends:
        - .macos_x86_64_aeva
        - .cmake_build_macos
        - .cmake_build_artifacts
        - .macos_builder_tags
        - .run_manually

test:macos-x86_64-aeva:
    extends:
        - .macos_x86_64_aeva
        - .cmake_test_macos
        - .cmake_package_artifacts
        - .macos_builder_tags
        - .run_automatically
    dependencies:
        - build:macos-x86_64-aeva
    needs:
        - build:macos-x86_64-aeva

upload:girder-macos-x86_64-aeva:
    extends:
        - .girder_upload
        - .upload_only
    variables:
        # AEVA/continuous
        GIRDER_TARGET_FOLDER: 5e7a46f6af2e2eed356a1836
        GIT_CLONE_PATH: $CI_BUILDS_DIR/gitlab-kitware-sciviz-ci
    dependencies:
        - test:macos-x86_64-aeva
    needs:
        - test:macos-x86_64-aeva

## Windows

### AEVA

build:windows-vs2019-aeva:
    extends:
        - .windows_vs2019_aeva
        - .cmake_build_windows
        - .cmake_build_artifacts
        - .windows_builder_tags_aeva
        - .run_manually
    # adding occt extends the build time
    timeout: 1h 30m

test:windows-vs2019-aeva:
    extends:
        - .windows_vs2019_aeva
        - .cmake_test_windows
        - .cmake_package_artifacts
        - .windows_builder_tags_aeva
        - .run_automatically
    dependencies:
        - build:windows-vs2019-aeva
    needs:
        - build:windows-vs2019-aeva

upload:girder-windows-vs2019-aeva:
    extends:
        - .girder_upload
        - .upload_only
    variables:
        # AEVA/continuous
        GIRDER_TARGET_FOLDER: 5e7a46f6af2e2eed356a1836
        GIT_CLONE_PATH: $CI_BUILDS_DIR/gitlab-kitware-sciviz-ci
    dependencies:
        - test:windows-vs2019-aeva
    needs:
        - test:windows-vs2019-aeva

# CI update jobs

## SMTK

### Linux

update-ci:smtk-fedora33:
    extends:
        - .image_builder
        - .upload_only
    needs:
        - test:centos7-modelbuilder
    script:
        - .gitlab/ci/sccache.sh
        - .gitlab/ci/build-smtk-image.sh fedora33 fedora33

update-ci:smtk-fedora33-paraview:
    extends:
        - .image_builder
        - .upload_only
    needs:
        - test:centos7-modelbuilder
    script:
        - .gitlab/ci/sccache.sh
        - .gitlab/ci/build-smtk-image.sh fedora33-paraview fedora33-paraview

update-ci:smtk-fedora33-vtk:
    extends:
        - .image_builder
        - .upload_only
    needs:
        - test:centos7-vtk
    script:
        - .gitlab/ci/sccache.sh
        - .gitlab/ci/build-smtk-image.sh fedora33-vtk fedora33-vtk

### macOS

update-ci:smtk-macos-arm64:
    extends:
        - .upload_developer_install
        - .upload_only
    dependencies:
        - build:macos-arm64-smtk
    needs:
        - build:macos-arm64-smtk
        - test:macos-arm64-smtk
    variables:
        TARGET_PROJECT: smtk
        PROJECTS_TO_CLEAN: "smtk"
        PREFIX: "/opt/glr/cmb/cmb-ci"
        # ComputationalModelBuilder/ci/smtk/macos/arm64
        GIRDER_TARGET_FOLDER: 60b8ff012fa25629b92ddbb5

update-ci:smtk-macos-x86_64:
    extends:
        - .upload_developer_install
        - .upload_only
    dependencies:
        - build:macos-x86_64-smtk
    needs:
        - build:macos-x86_64-smtk
        - test:macos-x86_64-smtk
    variables:
        TARGET_PROJECT: smtk
        PROJECTS_TO_CLEAN: "smtk"
        PREFIX: "/opt/glr/cmb/cmb-ci"
        # ComputationalModelBuilder/ci/smtk/macos
        GIRDER_TARGET_FOLDER: 5f0726729014a6d84e0e7cab

### Windows

update-ci:smtk-windows-vs2019:
    extends:
        - .upload_developer_install
        - .upload_only
    dependencies:
        - build:windows-vs2019-smtk
    needs:
        - build:windows-vs2019-smtk
        - test:windows-vs2019-smtk
    variables:
        TARGET_PROJECT: smtk
        PROJECTS_TO_CLEAN: "smtk"
        PREFIX: "C:/glr/builds/cmb/cmb-ci"
        # ComputationalModelBuilder/ci/smtk/windows/vs2019
        GIRDER_TARGET_FOLDER: 5f07268c9014a6d84e0e7cd7

## CMB

### Linux

update-ci:cmb-fedora33:
    extends:
        - .image_builder
        - .upload_only
    needs:
        - test:centos7-modelbuilder
    script:
        - .gitlab/ci/sccache.sh
        - .gitlab/ci/build-cmb-image.sh fedora33 fedora33

### macOS

update-ci:cmb-macos-arm64:
    extends:
        - .upload_developer_install
        - .upload_only
    dependencies:
        - build:macos-arm64-modelbuilder
    needs:
        - build:macos-arm64-modelbuilder
        - test:macos-arm64-modelbuilder
    variables:
        TARGET_PROJECT: cmb
        PROJECTS_TO_CLEAN: "cmb"
        PREFIX: "/opt/glr/cmb/cmb-ci"
        # ComputationalModelBuilder/ci/cmb/macos/arm64
        GIRDER_TARGET_FOLDER: 60b8ff802fa25629b92e0ea4

update-ci:cmb-macos-x86_64:
    extends:
        - .upload_developer_install
        - .upload_only
    dependencies:
        - build:macos-x86_64-modelbuilder
    needs:
        - build:macos-x86_64-modelbuilder
        - test:macos-x86_64-modelbuilder
    variables:
        TARGET_PROJECT: cmb
        PROJECTS_TO_CLEAN: "cmb"
        PREFIX: "/opt/glr/cmb/cmb-ci"
        # ComputationalModelBuilder/ci/cmb/macos
        GIRDER_TARGET_FOLDER: 5f07264e9014a6d84e0e7c5e

### Windows

update-ci:cmb-windows-vs2019:
    extends:
        - .upload_developer_install
        - .upload_only
    dependencies:
        - build:windows-vs2019-modelbuilder
    needs:
        - build:windows-vs2019-modelbuilder
        - test:windows-vs2019-modelbuilder
    variables:
        TARGET_PROJECT: cmb
        PROJECTS_TO_CLEAN: "cmb"
        PREFIX: "C:/glr/builds/cmb/cmb-ci"
        # ComputationalModelBuilder/ci/cmb/windows/vs2019
        GIRDER_TARGET_FOLDER: 5f07265b9014a6d84e0e7c7d

## AEVA

### Linux

update-ci:aevasession-fedora33:
    extends:
        - .image_builder
        - .upload_only
    needs:
        - test:centos7-aeva
    script:
        - .gitlab/ci/sccache.sh
        - .gitlab/ci/build-aevasession-image.sh fedora33 fedora33

update-ci:aeva-fedora33:
    extends:
        - .image_builder
        - .upload_only
    needs:
        - test:centos7-aeva
    script:
        - .gitlab/ci/sccache.sh
        - .gitlab/ci/build-aeva-image.sh fedora33 fedora33

### macOS

update-ci:aevasession-macos-x86_64:
    extends:
        - .upload_developer_install
        - .upload_only
    dependencies:
        - build:macos-x86_64-aeva
    needs:
        - build:macos-x86_64-aeva
        - test:macos-x86_64-aeva
    variables:
        TARGET_PROJECT: aevasession
        PROJECTS_TO_CLEAN: "aeva aevasession"
        PREFIX: "/opt/glr/cmb/cmb-ci"
        # AEVA/ci/session/macos
        GIRDER_TARGET_FOLDER: 5eff45299014a6d84ef503f2

update-ci:aeva-macos-x86_64:
    extends:
        - .upload_developer_install
        - .upload_only
    dependencies:
        - build:macos-x86_64-aeva
    needs:
        - build:macos-x86_64-aeva
        - test:macos-x86_64-aeva
    variables:
        TARGET_PROJECT: aeva
        PROJECTS_TO_CLEAN: "aeva"
        PREFIX: "/opt/glr/cmb/cmb-ci"
        # AEVA/ci/macos
        GIRDER_TARGET_FOLDER: 5efcaf959014a6d84eeeae22

### Windows

update-ci:aevasession-windows-vs2019:
    extends:
        - .upload_developer_install
        - .upload_only
    dependencies:
        - build:windows-vs2019-aeva
    needs:
        - build:windows-vs2019-aeva
        - test:windows-vs2019-aeva
    variables:
        TARGET_PROJECT: aevasession
        PROJECTS_TO_CLEAN: "aeva aevasession"
        PREFIX: "C:/glr/builds/cmb/cmb-ci"
        # AEVA/ci/session/windows/vs2019
        GIRDER_TARGET_FOLDER: 5eff45339014a6d84ef503fb

update-ci:aeva-windows-vs2019:
    extends:
        - .upload_developer_install
        - .upload_only
    dependencies:
        - build:windows-vs2019-aeva
    needs:
        - build:windows-vs2019-aeva
        - test:windows-vs2019-aeva
    variables:
        TARGET_PROJECT: aeva
        PROJECTS_TO_CLEAN: "aeva"
        PREFIX: "C:/glr/builds/cmb/cmb-ci"
        # AEVA/ci/windows/vs2019
        GIRDER_TARGET_FOLDER: 5efcafa39014a6d84eeeae2b
