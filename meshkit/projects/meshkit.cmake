superbuild_add_project(meshkit
  DEPENDS moab cgm
  CMAKE_ARGS
    # Set link path on linux
    -DCMAKE_INSTALL_RPATH:PATH=$ORIGIN/../lib
    -DCGM_DIR:PATH=<INSTALL_DIR>/lib/cmake/CGM
    -DBUILD_ALGS:BOOL=ON
    -DBUILD_SRC:BOOL=ON
    -DBUILD_UTILS:BOOL=ON
    -DBUILD_RGG:BOOL=ON
    -DWITH_MPI:BOOL=OFF
    -DENABLE_TESTING:BOOL=OFF)

# MOAB has public headers which include Eigen, but it doesn't add the include
# flags properly.
superbuild_append_flags(cxx_flags "-I<INSTALL_DIR>/include/eigen3" PROJECT_ONLY)
