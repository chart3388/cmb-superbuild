# Build and install:
superbuild_add_project(pegtl
  DEPENDS cxx11
  CMAKE_ARGS
    -DPEGTL_BUILD_EXAMPLES:BOOL=OFF
    -DPEGTL_BUILD_TESTS:BOOL=OFF
)

# Provide our location to dependent projects:
superbuild_add_extra_cmake_args(
  -Dpegtl_DIR:PATH=<INSTALL_DIR>/share/pegtl/cmake
)
