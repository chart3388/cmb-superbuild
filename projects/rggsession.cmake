set(extra_cmake_args)
if (UNIX AND NOT APPLE)
  list(APPEND extra_cmake_args
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=FALSE)
endif ()

if (cubit_enabled)
  list(APPEND extra_cmake_args
  -DENABLE_CUBIT_BINDINGS:BOOL=ON
  -DCUBIT_EXE:PATH=${CUBIT_EXE}
  )
endif ()

if (meshkit_enabled)
  list(APPEND extra_cmake_args
    -DASSYGEN_EXE:PATH=<INSTALL_DIR>/meshkit/bin/assygen
    -DCOREGEN_EXE:PATH=<INSTALL_DIR>/meshkit/bin/coregen
    )
endif ()

superbuild_add_project(rggsession
  DEBUGGABLE
  DEPENDS boost cxx11 libarchive paraview qt5 smtk
  DEPENDS_OPTIONAL pyarc python python2 python3 meshkit cubit
  CMAKE_ARGS
    ${extra_cmake_args}
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
    -DENABLE_PYARC_BINDINGS:BOOL=${pyarc_enabled}
    -DENABLE_PYTHON_WRAPPING:BOOL=${python_enabled})

superbuild_add_extra_cmake_args(
  -DSIMULATION_WORKFLOWS_ROOT:PATH=<INSTALL_DIR>/share/cmb/workflows)

superbuild_declare_paraview_xml_files(rggsession
  FILE_NAMES "smtk.rggsession.xml"
  DIRECTORY_HINTS "smtk-${smtk_version}")
