superbuild_add_project(meshkit
  CMAKE_ARGS
    -Dsuperbuild_install_location:PATH=<INSTALL_DIR>/meshkit
    -Dsuperbuild_download_location:PATH=${superbuild_download_location}
    -DUSE_SYSTEM_zlib:BOOL=${USE_SYSTEM_zlib}
  INSTALL_COMMAND
    "")
