set(smtkace3p_rpaths)
if (APPLE AND USE_SYSTEM_qt5)
  # On macOS, Qt5 packages use `@rpath` as their library ids. Add an rpath for
  # it to the build.
  list(APPEND smtkace3p_rpaths
    "${qt5_rpath}")
endif ()
string(REPLACE ";" "${_superbuild_list_separator}"
  smtkace3p_rpaths
  "${smtkace3p_rpaths}")

set(smtkace3p_extra_cmake_options)

set(smtkace3p_enable_by_default OFF)
if ("${SUPERBUILD_PACKAGE_MODE}" STREQUAL "ace3p")
  set(smtkace3p_enable_by_default ON)
  list(APPEND smtkace3p_extra_cmake_options
    -DENABLE_ACE3P_UI_FEATURES:BOOL=ON)
endif()

superbuild_add_project(smtkace3p
  DEBUGGABLE
  DEFAULT_ON
  DEPENDS boost cumulus cxx11 paraview python qt5 smtk
  DEPENDS_OPTIONAL python2 python3
  CMAKE_ARGS
    ${smtkace3p_extra_cmake_options}
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -DCMAKE_INSTALL_RPATH:STRING=${smtkace3p_rpaths}
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
    -DSIMULATION_WORKFLOWS_ROOT:PATH=<INSTALL_DIR>/share/cmb/workflows
    -DENABLE_PLUGIN_BY_DEFAULT=${smtkace3p_enable_by_default}
)

superbuild_declare_paraview_xml_files(smtkace3p
  FILE_NAMES "smtk.ace3p.xml"
  DIRECTORY_HINTS "smtk-${smtk_version}")
