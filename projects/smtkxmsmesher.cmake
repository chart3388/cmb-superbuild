superbuild_add_project(smtkxmsmesher
  DEBUGGABLE
  DEPENDS smtk xmsmesher
  DEPENDS_OPTIONAL pybind11 python2 python3
  CMAKE_ARGS
    -Wno-dev
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DCMAKE_INSTALL_CMBWORKFLOWDIR:PATH=<INSTALL_DIR>/share/cmb/workflows
    -DBUILD_TESTING:BOOL=OFF
    -DENABLE_PYTHON_WRAPPING:BOOL=${pybind11_enabled}
    -DPYTHON_TARGET_VERSION:STRING=${superbuild_python_version}
    )

superbuild_declare_paraview_xml_files(smtkxmsmesher
  FILE_NAMES "smtk.xmsmeshoperation.xml"
  DIRECTORY_HINTS "smtk-${smtk_version}")
